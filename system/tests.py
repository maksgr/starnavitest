from django.urls import reverse
from rest_framework.test import APITestCase
from django.contrib.auth.models import User
from rest_framework import status


class SystemPostTest(APITestCase):
    def setUp(self):
        self.post_url = reverse('ListTodo')
        self.signup_url = "http://127.0.0.1:8000/rest-auth/login/"

    def test_show_post(self):
        response = self.client.get(self.post_url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_post(self):
        data = {
            'title': 'test',
            'description': 'test' * 100
        }

        response = self.client.post(self.post_url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['title'], data['title'])
        self.assertEqual(response.data['description'], data['description'])

    def test_signup(self):
        user = User.objects.create_user('root', 'root@gmail.com', 'root1111')
        self.client.force_authenticate(user)

        response = self.client.get(self.post_url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
