from rest_framework import generics
from system import models
from system.serializers import PostSerializer


class ListTodo(generics.ListCreateAPIView):
    queryset = models.Post.objects.all()
    serializer_class = PostSerializer


class DetailTodo(generics.RetrieveUpdateDestroyAPIView):
    queryset = models.Post.objects.all()
    serializer_class = PostSerializer
