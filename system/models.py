from django.db import models
from django.contrib.auth.models import User


class Post(models.Model):
    title = models.CharField(verbose_name="Post title", max_length=50)
    description = models.TextField(verbose_name="Description of the post")
    created_at = models.DateTimeField(verbose_name="Date and time the post was created", auto_now_add=True, null=True)
    created_by = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return '%s (%s)' % (self.title, self.created_by)
