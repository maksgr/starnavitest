from django.apps import AppConfig


class StystemConfig(AppConfig):
    name = 'system'
