from config import number_of_users, max_post_pre_user, max_likes_per_user
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
import random


class Bot(APIView):
    def get(self, request):
        user = random.randint(1, number_of_users)
        max_post = random.randint(1, max_post_pre_user)
        max_likes = random.randint(1, max_likes_per_user)

        print(user, max_post, max_likes)

        return Response(status.HTTP_201_CREATED)
